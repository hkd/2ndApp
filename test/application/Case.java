/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

/**
 *
 * @author HKD
 */
public class Case {
    /**
     * C'est le contenu des etapes
     */
    public String content;
    /**
     * C'est le resultat des etapes
     */
    public String expected;

    public Case(String step, String resultat) {
        this.content = step;
        this.expected = resultat;
    }

    @Override
    public String toString() {
        return "{"
                    +"\"expected\":\""+expected +"\","
                    +"\"content\":\""+content+"\"}";
    }
    
    
    
}
