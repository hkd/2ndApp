/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import com.gurock.testrail.APIClient;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import javax.xml.soap.Node;
import org.json.simple.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit5.ApplicationTest;

/**
 *
 * @author HKD
 */
public class MainTest extends ApplicationTest {

    /**
     * Temps d'attend avant action
     */
    private int tmp = 500;
    /**
     * Client de testrail. permet de synchroniser les resultats directement sur
     * testrail
     */
    
    /**
     * recuperer le resultField et faire la verification des elements attendus
     */
    @FXML
    private TextArea resultField;
    @FXML
    private TextField firstOp;
    @FXML
    private TextField secondOp;
    @FXML
    private TextField thirdOp;
    //Control Button
    @FXML
    private Button lngButton;
    /**
     * Verifier s'il n ya pas eu une erreure precedente, si oui on redemarer le
     * systeme et on continu
     *
     */
    
    public MainTest() {
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.show();
    }

    @BeforeClass
    public static void setUpClass() {
        try {
            ApplicationTest.launch(Main.class);
        } catch (Exception ex) {
            Logger.getLogger(MainTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws Exception {
        resultField = findTextArea("#resultField");
        firstOp = findTextArea("#firstOp");
        secondOp = findTextArea("#secondOp");
        thirdOp = findTextArea("#thirdOp");
        lngButton = findTextArea("#lngButton");
    }

    @After
    public void tearDown() throws TimeoutException {
        FxToolkit.hideStage();
        FxToolkit.showStage();
    }

    @After
    public void atferEachTest() throws TimeoutException {

    }

    public <T extends Node> T find(final String query) {
        return (T) lookup(query).queryAll().iterator().next();
    }

    public <T extends Object> T findTextArea(final String query) {
        return (T) lookup(query).queryAll().iterator().next();
    }

    /**
     * @since permet de bien initialise
     */
    public void clear() {
        firstOp.setText("");
        secondOp.setText("");
        thirdOp.setText("");
    }

    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    /**
     * @since test avec des paramettres vide
     * @throws IOException
     * @throws ComparisonFailure 
     */
    @Test
    public void resolutionErrorFrOrEngNothingTest() throws IOException,ComparisonFailure{
        System.out.println("application.MainTest.resolutionErrorFrOrEngNothingTest()");
        clickOn("#clearButton").sleep(tmp);
        clear();
        clickOn("#firstOp").sleep(tmp);
        clickOn("#secondOp").sleep(tmp);
        clickOn("#thirdOp").sleep(tmp);
        clickOn("#submitButton").sleep(tmp);
        press(KeyCode.ENTER);
        Assert.assertEquals(resultField.getText(),"");
    }
    /**
     * @since test avec des nombres entiers mais la langue est en francais
     * @throws IOException
     * @throws ComparisonFailure 
     */
    @Test
    public void resolutionSuccesFrTest() throws IOException,ComparisonFailure {
        System.out.println("application.MainTest.resolutionSuccesFrTest()");
        // par defaut la langue est en français
        if (!lngButton.getText().contains("FR")) {
            clickOn("#lngButton").sleep(tmp);
        }

        // Le bouton entraine une erreure d'ou nous allons le controller
        clickOn("#clearButton").sleep(tmp);
        clear();
        clickOn("#firstOp").sleep(tmp);
        write("1").sleep(tmp);
        clickOn("#secondOp").sleep(tmp);
        write("2").sleep(tmp);
        clickOn("#thirdOp").sleep(tmp);
        write("1").sleep(tmp);
        clickOn("#submitButton").sleep(tmp);
        press(KeyCode.ENTER);
        Assert.assertEquals(resultField.getText(), "Le systeme admet une solution double x0 :-1.0 S = {-1.0}");

        clickOn("#clearButton").sleep(tmp);
        clear();
        clickOn("#firstOp").sleep(tmp);
        write("2").sleep(tmp);
        clickOn("#secondOp").sleep(tmp);
        write("2").sleep(tmp);
        clickOn("#thirdOp").sleep(tmp);
        write("1").sleep(tmp);
        clickOn("#submitButton").sleep(tmp);
        press(KeyCode.ENTER);
        Assert.assertEquals(resultField.getText(), "Le systeme n'admet pas de solutions dans R");

        clickOn("#clearButton").sleep(tmp);
        clear();
        clickOn("#firstOp").sleep(tmp);
        write("1");
        clickOn("#secondOp").sleep(tmp);
        write("5");
        clickOn("#thirdOp").sleep(tmp);
        write("6");
        clickOn("#submitButton").sleep(tmp);
        press(KeyCode.ENTER);
        Assert.assertEquals(resultField.getText(), "Les solutions sont x1: -3.0 x2: -2.0 S = {-3.0 , -2.0}");
    }
    
    /**
     * @since test avec des nombres entiers mais la langue est en anglais
     * @throws IOException
     * @throws ComparisonFailure 
     */
    @Test
    public void resolutionSuccesENGTest() throws IOException,ComparisonFailure {
        System.out.println("application.MainTest.resolutionSuccesENGTest()");
        // Nous mettons la langue en anglais
        if (lngButton.getText() != "ENG") {
            clickOn("#lngButton").sleep(tmp);
        }

        // Le bouton entraine une erreure d'ou nous allons le controller
        clickOn("#clearButton").sleep(tmp);
        clear();
        clickOn("#firstOp").sleep(tmp);
        write("1");
        clickOn("#secondOp").sleep(tmp);
        write("2");
        clickOn("#thirdOp").sleep(tmp);
        write("1");
        clickOn("#submitButton").sleep(tmp);
        press(KeyCode.ENTER);
        Assert.assertEquals(resultField.getText(), "Double solution x0 :-1.0S = {-1.0}");

        clickOn("#clearButton").sleep(tmp);
        clear();
        clickOn("#firstOp").sleep(tmp);
        write("2");
        clickOn("#secondOp").sleep(tmp);
        write("2");
        clickOn("#thirdOp").sleep(tmp);
        write("1");
        clickOn("#submitButton").sleep(tmp);
        press(KeyCode.ENTER);
        Assert.assertEquals(resultField.getText(), "There is no solutions in R");

        clickOn("#clearButton").sleep(tmp);
        clear();
        clickOn("#firstOp").sleep(tmp);
        write("1");
        clickOn("#secondOp").sleep(tmp);
        write("5");
        clickOn("#thirdOp").sleep(tmp);
        write("6");
        clickOn("#submitButton").sleep(tmp);
        press(KeyCode.ENTER);
        Assert.assertEquals(resultField.getText(), "Solutions are x1: -3.0 x2: -2.0 S = {-3.0 , -2.0}");
    }
    /**
     * @since test avec des nombres a caractere
     * @throws IOException
     * @throws ComparisonFailure 
     */
    @Test
    public void resolutionErrorFrOrEngCaractereTest() throws IOException,ComparisonFailure {
        System.out.println("application.MainTest.resolutionErrorFrOrEngCaractereTest()");
        clickOn("#clearButton").sleep(tmp);
        clear();
        clickOn("#firstOp").sleep(tmp);
        write("a");
        clickOn("#secondOp").sleep(tmp);
        write("b");
        clickOn("#thirdOp").sleep(tmp);
        write("c");
        clickOn("#submitButton").sleep(tmp);
        press(KeyCode.ENTER);
        Assert.assertEquals(resultField.getText(), "");
    }

    /**
     * @since test avec des nombres a virgule
     * @throws IOException
     * @throws ComparisonFailure 
     */
    @Test
    public void resolutionErrorFrOrEngDoubleTest() throws IOException,ComparisonFailure {
        System.out.println("application.MainTest.resolutionErrorFrOrEngDoubleTest()");
        clickOn("#clearButton").sleep(tmp);
        clear();
        clickOn("#firstOp").sleep(tmp);
        write("0.5");
        clickOn("#secondOp").sleep(tmp);
        write("1");
        clickOn("#thirdOp").sleep(tmp);
        write("0.5");
        clickOn("#submitButton").sleep(tmp);
        press(KeyCode.ENTER);
        Assert.assertEquals(resultField.getText(), "");
    }
}
