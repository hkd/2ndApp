/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import com.gurock.testrail.APIClient;
import com.gurock.testrail.APIException;
import java.io.IOException;
import java.util.ArrayList;
import org.json.simple.JSONObject;
import org.junit.ComparisonFailure;

/**
 *
 * @author HKD
 */
public class Test {

    public static APIClient client;
    MainTest mainTest;

    public Test() throws Exception {
        mainTest = new MainTest();
        MainTest.setUpClass();
        mainTest.setUp();
        client = new APIClient("http://localhost/testrail-dao/");
        client.setUser("daohamadou@gmail.com");
        client.setPassword("safari");
        resolutionErrorFrOrEngNothingTestSynchronisation();
        resolutionSuccesFrTestSynchronisation();
        resolutionSuccesENGTestSynchronisation();
        resolutionErrorFrOrEngCaractereTestSynchronisation();
        resolutionErrorFrOrEngDoubleTestSynchronisation();
    }

    public void updateCase(String c_id, JSONObject object) {
        try {
            client.sendPost("update_case/" + c_id, object);
        } catch (IOException | APIException ex) {
            System.err.println("Impossible de mettre a jour" + ex.getMessage());
        }
    }

    public JSONObject getCase(String c_id) {

        try {
            System.out.println("application.MainTest.getCase()");
            System.out.println((JSONObject) client.sendGet("get_case/" + c_id));
            return (JSONObject) client.sendGet("get_case/" + c_id);
        } catch (IOException | APIException ex) {
            System.err.println(ex.getMessage());
        }
        System.out.println("application.MainTest.getCase()error");
        return new JSONObject();

    }

    private void resolutionErrorFrOrEngNothingTestSynchronisation() {
        long time = System.currentTimeMillis();
        String precondition = "Nous lançons l'application et nous cliquons sur le bouton de resolution\n"
                + "Les valeurs des champs restent vide ";
        String resultat = "";
        JSONObject c = getCase("14");
        String stepString = "";
        stepString += "cliquer sur clearButton\n";
        stepString += "cliquer sur firstOp\n";
        stepString += "cliquer sur secondOp\n";
        stepString += "cliquer sur thirdOp\n";
        stepString += "cliquer sur submitButton\n";
        System.out.println("application.MainTest.resolutionErrorFrOrEngNothingTest()");
        try {
            mainTest.resolutionErrorFrOrEngNothingTest();
            resultat = "Resultat attendu vide \nSuccess";
        } catch (ComparisonFailure ex) {
            resultat = "Resultat non attendu \n" + ex.getMessage();
        } catch (IOException ex) {
            resultat = "Resultat non attendu \n" + ex.getMessage();
        }
        time = System.currentTimeMillis() - time;
        c.put("custom_steps", stepString);
        c.put("estimate", (time / 1000) + "s");
        c.put("custom_preconds", precondition);
        c.put("custom_expected", resultat);
        updateCase("14", c);
    }
    
    private void resolutionSuccesFrTestSynchronisation() {
        String id = "15";
        long time = System.currentTimeMillis();
        String precondition = "Nous lançons l'application et nous cliquons sur le bouton de resolution\n"
                + "Les valeurs des champs restent vide ";
        JSONObject c = getCase(id);
        ArrayList<String> steps = new ArrayList<>();
        ArrayList<String> resultats = new ArrayList<>();
        String step = "";
        String resultat = "";
        step += "clic sur le bouton langue si bouton n'est pas FR\\n";
        step += "cliquer sur clearButton\\n";
        step += "click sur firstOp\\n";
        step += "ecrire 1\\n";
        step += "click sur secondOp\\n";
        step += "ecrire 2\\n";
        step += "click sur thirdOp\\n";
        step += "ecrire 1\\n";
        step += "click sur submitButton\\n";
        steps.add(step);
        step = "";
        step += "clic sur le bouton langue si bouton n'est pas FR\\n";
        step += "cliquer sur clearButton\\n";
        step += "click sur firstOp\\n";
        step += "ecrire 2\\n";
        step += "click sur secondOp\\n";
        step += "ecrire 2\\n";
        step += "click sur thirdOp\\n";
        step += "ecrire 1\\n";
        step += "click sur submitButton\\n";
        steps.add(step);
        step = "";
        step += "clic sur le bouton langue si bouton n'est pas FR\\n";
        step += "cliquer sur clearButton\\n";
        step += "click sur firstOp\\n";
        step += "ecrire 1\\n";
        step += "click sur secondOp\\n";
        step += "ecrire 5\\n";
        step += "click sur thirdOp\\n";
        step += "ecrire 6\\n";
        step += "click sur submitButton\\n";
        steps.add(step);
        try {
            mainTest.resolutionSuccesFrTest();
            resultat = "Resultat attendu : Le systeme admet une solution double x0 :-1.0"
                    + " S = {-1.0}\\nsuccess";
            resultats.add(resultat);
            resultat = "Resultat attendu : Le systeme n'admet pas de solutions dans R\\nsuccess";
            resultats.add(resultat);
            resultat = "Resultat attendu : Les solutions sont x1: -3.0 x2: -2.0 S = {-3.0 , -2.0}\\nsuccess";
            resultats.add(resultat);
        } catch (IOException | ComparisonFailure ex) {
            resultat = "Resultat attendu : Le systeme admet une solution double x0 :-1.0"
                    + " S = {-1.0}\\n#echec";
            resultats.add(resultat);
            resultat = "Resultat attendu : Le systeme n'admet pas de solutions dans R\n#echec";
            resultats.add(resultat);
            resultat = "Resultat attendu : Les solutions sont x1: -3.0 x2: -2.0 S = {-3.0 , -2.0}\\n#echec";
            resultats.add(resultat);
        }
        ArrayList<Case> liste = new ArrayList<>();
        liste.add(new Case(steps.get(0), resultats.get(0)));
        liste.add(new Case(steps.get(1), resultats.get(1)));
        liste.add(new Case(steps.get(2), resultats.get(2)));
        time = System.currentTimeMillis() - time;
        c.put("custom_steps_separated", liste);
        c.put("custom_preconds", precondition);
        c.put("estimate", (time / 1000) + "s");
        c.put("custom_expected", resultat);
        updateCase(id, c);

    }

    private void resolutionSuccesENGTestSynchronisation() {
        String id = "16";
        long time = System.currentTimeMillis();
        String precondition = "Nous lançons l'application et verifions la langue\n"
                + "Les valeurs des champs restent vide ";
        JSONObject c = getCase(id);
        ArrayList<String> steps = new ArrayList<>();
        ArrayList<String> resultats = new ArrayList<>();
        String step = "";
        String resultat = "";
        step += "clic sur le bouton langue si bouton n'est pas ENG\\n";
        step += "cliquer sur clearButton\\n";
        step += "click sur firstOp\\n";
        step += "ecrire 1\\n";
        step += "click sur secondOp\\n";
        step += "ecrire 2\\n";
        step += "click sur thirdOp\\n";
        step += "ecrire 1\\n";
        step += "click sur submitButton\\n";
        steps.add(step);
        step = "";
        step += "cliquer sur clearButton\\n";
        step += "click sur firstOp\\n";
        step += "ecrire 2\\n";
        step += "click sur secondOp\\n";
        step += "ecrire 2\\n";
        step += "click sur thirdOp\\n";
        step += "ecrire 1\\n";
        step += "click sur submitButton\\n";
        steps.add(step);
        step = "";
        step += "cliquer sur clearButton\\n";
        step += "click sur firstOp\\n";
        step += "ecrire 1\\n";
        step += "click sur secondOp\\n";
        step += "ecrire 5\\n";
        step += "click sur thirdOp\\n";
        step += "ecrire 6\\n";
        step += "click sur submitButton\\n";
        steps.add(step);
        try {
            mainTest.resolutionSuccesENGTest();
            resultat = "Resultat attendu : Double solution x0 :-1.0S = {-1.0}"
                    + "\\nsuccess";
            resultats.add(resultat);
            resultat = "Resultat attendu : There is no solutions in R\\nsuccess";
            resultats.add(resultat);
            resultat = "Resultat attendu : Solutions are x1: -3.0 x2: -2.0 S = {-3.0 , -2.0}\\nsuccess";
            resultats.add(resultat);
        } catch (IOException | ComparisonFailure ex) {
            resultat = "Resultat attendu : Double solution x0 :-1.0S = {-1.0}"
                    + " \\n#echec";
            resultats.add(resultat);
            resultat = "Resultat attendu : There is no solutions in R\n#echec";
            resultats.add(resultat);
            resultat = "Resultat attendu : Solutions are x1: -3.0 x2: -2.0 S = {-3.0 , -2.0}\\n#echec";
            resultats.add(resultat);
        }
        ArrayList<Case> liste = new ArrayList<>();
        liste.add(new Case(steps.get(0), resultats.get(0)));
        liste.add(new Case(steps.get(1), resultats.get(1)));
        liste.add(new Case(steps.get(2), resultats.get(2)));
        time = System.currentTimeMillis() - time;
        c.put("custom_steps_separated", liste);
        c.put("custom_preconds", precondition);
        c.put("estimate", (time / 1000) + "s");
        c.put("custom_expected", resultat);
        updateCase(id, c);

    }

    private void resolutionErrorFrOrEngCaractereTestSynchronisation() {
        long time = System.currentTimeMillis();
        String precondition = "Nous lançons l'application et saisissons des caracteres au niveau de champs";
        String id="17";
        String resultat = "";
        JSONObject c = getCase(id);
        String stepString = "";
        stepString += "cliquer sur clearButton\n";
        stepString += "cliquer sur firstOp\n";
        stepString += "Ecrire a\n";
        stepString += "cliquer sur secondOp\n";
        stepString += "ecrire b\n";
        stepString += "cliquer sur thirdOp\n";
        stepString += "ecrire c\n";
        stepString += "cliquer sur submitButton\n";
        System.out.println("application.Test.resolutionErrorFrOrEngCaractereTestSynchronisation()");
        try {
            mainTest.resolutionErrorFrOrEngCaractereTest();
            resultat = "Resultat attendu vide \nSuccess";
        } catch (ComparisonFailure ex) {
            resultat = "Resultat non attendu \n" + ex.getMessage();
        } catch (IOException ex) {
            resultat = "Resultat non attendu \n" + ex.getMessage();
        }
        time = System.currentTimeMillis() - time;
        c.put("custom_steps", stepString);
        c.put("estimate", (time / 1000) + "s");
        c.put("custom_expected", resultat);
        c.put("custom_preconds", precondition);
        updateCase(id, c);
    }

 private void resolutionErrorFrOrEngDoubleTestSynchronisation() {
        long time = System.currentTimeMillis();
        String precondition = "Nous lançons l'application et saisissons des nombres a virgule"
                + " au niveau de champs";
        String id="18";
        String resultat = "";
        JSONObject c = getCase(id);
        String stepString = "";
        stepString += "cliquer sur clearButton\n";
        stepString += "cliquer sur firstOp\n";
        stepString += "Ecrire 0.5\n";
        stepString += "cliquer sur secondOp\n";
        stepString += "ecrire 1\n";
        stepString += "cliquer sur thirdOp\n";
        stepString += "ecrire 0.5\n";
        stepString += "cliquer sur submitButton\n";
        System.out.println("application.Test.resolutionErrorFrOrEngDoubleTestSynchronisation()");
        try {
            mainTest.resolutionErrorFrOrEngCaractereTest();
            resultat = "Resultat attendu vide \nSuccess";
        } catch (ComparisonFailure ex) {
            resultat = "Resultat non attendu \n" + ex.getMessage();
        } catch (IOException ex) {
            resultat = "Resultat non attendu \n" + ex.getMessage();
        }
        time = System.currentTimeMillis() - time;
        c.put("custom_steps", stepString);
        c.put("estimate", (time / 1000) + "s");
        c.put("custom_expected", resultat);
        c.put("custom_preconds", precondition);
        updateCase(id, c);
    }

 
    public static void main(String[] args) throws Exception {
        new Test();
    }
}
