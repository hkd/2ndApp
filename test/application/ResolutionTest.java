/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.io.IOException;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author HKD
 */
public class ResolutionTest {

    private String dte;
    private String equation;
    private String solution;
    private Resolution resolution;

    public ResolutionTest() {
        resolution = new Resolution(dte, equation, solution);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        solution="Cette equation admet une solution double";
        dte = dte = new Date().toString();
        equation = "xe+2x+1";
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void setDteTest() throws IOException {
        System.out.println("application.Resolution.setDte()");
        resolution.setDte(dte);
        assertEquals(dte, resolution.getDte());
    }
    @Test
    public void setEquTest() throws IOException {
        System.out.println("application.Resolution.setEqu()");        
        resolution.setEqu(equation);
        assertEquals(equation, resolution.getEqu());
    }
    @Test
    public void setSolutionTest() throws IOException {
        System.out.println("application.Resolution.setSolution()");
        resolution.setSolution(solution);
        assertEquals(solution, resolution.getSolution());
    }
}
